# Éditeur de Fichier Texte

Ce projet est un éditeur de fichier texte développé en C# avec Windows Forms sous Microsoft Visual Studio. Il a été conçu comme un exercice pratique pour découvrir l'environnement de développement Visual Studio et se familiariser avec la création d'applications Windows Form.

## Installation

Pour utiliser cet éditeur de texte, assurez-vous d'avoir Microsoft Visual Studio 2019

### Prérequis

- Microsoft Visual Studio 2019
- .NET Framework (version compatible avec Visual Studio)

### Étapes d'installation

1. Clonez le dépôt ou téléchargez le code source.
2. Ouvrez le fichier de solution (`*.sln`) avec Visual Studio.
3. Compilez le projet en choisissant `Build > Build Solution`.
4. Lancez l'application en choisissant `Debug > Start Without Debugging`.

## Utilisation

L'application permet de créer, ouvrir, éditer et sauvegarder des fichiers texte. Voici les principales fonctionnalités :

- **Ouvrir un fichier texte :** Permet de sélectionner et ouvrir un fichier texte existant.
- **Enregistrer sous… :** Sauvegarde le contenu actuel dans un fichier texte, avec possibilité de choisir le chemin et le nom du fichier.
- **Personnalisation :** Permet de personnaliser la police et la couleur du texte via des menus dédiés.

## Fonctionnalités

- Fenêtre principale de 700x500 pixels, centrée à l'écran.
- Menu Fichier avec les options Ouvrir et Enregistrer sous.
- Zone de texte multiligne prenant en charge le redimensionnement dynamique.
- Confirmation de fermeture si des modifications n'ont pas été enregistrées.
- Détecte si le fichier ouvert a été modifié depuis une autre source.
- Personnalisation de la police et de la couleur du texte persistante entre les sessions.
- Prise en charge de l'ouverture de fichiers via la ligne de commande ou par glisser-déposer sur l'icône de l'application.

## Contact

Nathan Fontaine - fontaine.nathan2002@gmail.com
