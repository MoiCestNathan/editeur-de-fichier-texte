﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{

    public partial class Form1 : Form
    {
        private FileSystemWatcher fileWatcher;
        private DateTime lastWriteTime;
        private string currentFilePath;

        public Form1(string filePath = "")
        {
            InitializeComponent();

            fileWatcher = new FileSystemWatcher();
            fileWatcher.NotifyFilter = NotifyFilters.LastWrite;
            fileWatcher.Changed += OnChanged;

            if (Properties.Settings.Default.WindowSize != new Size(0, 0))
            {
                this.Size = Properties.Settings.Default.WindowSize;
                this.WindowState = Properties.Settings.Default.WindowState;
            }

            AdjustWindowPosition();

            if (Properties.Settings.Default.TextFont != null)
            {
                textBox1.Font = Properties.Settings.Default.TextFont;
            }
            if (Properties.Settings.Default.TextColor != null)
            {
                textBox1.ForeColor = Properties.Settings.Default.TextColor;
            }

            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
            }

            if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                OpenFile(filePath);
            }
        }

        private void AdjustWindowPosition()
        {
            Screen screen = Screen.FromControl(this);
            Rectangle workingArea = screen.WorkingArea;
            if (Properties.Settings.Default.WindowLocation.X < workingArea.X ||
                Properties.Settings.Default.WindowLocation.Y < workingArea.Y ||
                Properties.Settings.Default.WindowLocation.X > workingArea.Right - 100 ||
                Properties.Settings.Default.WindowLocation.Y > workingArea.Bottom - 100)
            {
                this.Location = new Point(workingArea.X, workingArea.Y);
            }
            else
            {
                this.Location = Properties.Settings.Default.WindowLocation;
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ouvrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Fichiers texte (*.txt)|*.txt"; 
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        string fileContent = File.ReadAllText(openFileDialog.FileName);

                        textBox1.Text = fileContent;

                        string filename = Path.GetFileName(openFileDialog.FileName);

                        lastWriteTime = File.GetLastWriteTime(openFileDialog.FileName);
                        currentFilePath = openFileDialog.FileName;

                        fileWatcher.Path = Path.GetDirectoryName(currentFilePath);
                        fileWatcher.Filter = Path.GetFileName(currentFilePath);
                        fileWatcher.EnableRaisingEvents = true;

                        this.Text = $"Éditeur de fichier texte [{filename}]";
                    }
                    catch (IOException ioEx)
                    {
                        MessageBox.Show("Erreur lors de la lecture du fichier : " + ioEx.Message);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Une erreur est survenue : " + ex.Message);
                    }
                }
            }
        }
        private void enregistrerSousToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "Fichiers texte (*.txt)|*.txt";
                saveFileDialog.RestoreDirectory = true;
                saveFileDialog.OverwritePrompt = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        File.WriteAllText(saveFileDialog.FileName, textBox1.Text);
                        
                        this.Text = this.Text.TrimEnd('*');

                        lastWriteTime = File.GetLastWriteTime(saveFileDialog.FileName);
                        currentFilePath = saveFileDialog.FileName;

                    }
                    catch (IOException ioEx)
                    {
                        MessageBox.Show("Erreur lors de l'enregistrement du fichier : " + ioEx.Message);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Une erreur est survenue : " + ex.Message);
                    }
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (!this.Text.EndsWith("*"))
            {
                this.Text += "*";
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.WindowSize = this.Size;
            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.WindowState = this.WindowState;
            Properties.Settings.Default.Save();

            fileWatcher.EnableRaisingEvents = false;

            if (this.Text.EndsWith("*"))
            {
                var result = MessageBox.Show("Vous avez des modifications non enregistrées. Voulez-vous vraiment quitter sans enregistrer ?", "Confirmer la fermeture", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (e.FullPath == currentFilePath)
            {
                Invoke(new Action(() =>
                {
                    if (!this.Text.EndsWith("*") && File.GetLastWriteTime(currentFilePath) != lastWriteTime)
                    {
                        var result = MessageBox.Show("Le fichier a été modifié en dehors de l'éditeur. Voulez-vous recharger le fichier ?", "Fichier modifié", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            lastWriteTime = File.GetLastWriteTime(currentFilePath); 
                            textBox1.Text = File.ReadAllText(currentFilePath);
                            this.Text = $"Éditeur de fichier texte [{Path.GetFileName(currentFilePath)}]";
                        }
                    }
                }));
            }
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(currentFilePath) && File.Exists(currentFilePath))
            {
                var fileLastWriteTime = File.GetLastWriteTime(currentFilePath);
                if (fileLastWriteTime != lastWriteTime && !this.Text.EndsWith("*"))
                {
                    var result = MessageBox.Show("Le fichier a été modifié en dehors de l'éditeur. Voulez-vous recharger le fichier ?", "Fichier modifié", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        lastWriteTime = fileLastWriteTime;
                        textBox1.Text = File.ReadAllText(currentFilePath);
                        this.Text = $"Éditeur de fichier texte [{Path.GetFileName(currentFilePath)}]";
                    }
                }
                else if (fileLastWriteTime != lastWriteTime && this.Text.EndsWith("*"))
                {
                    var userChoice = MessageBox.Show("Le fichier a été modifié depuis la dernière sauvegarde et en dehors de l'éditeur. Voulez-vous recharger le fichier et perdre les modifications non enregistrées ?", "Fichier modifié", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                    if (userChoice == DialogResult.Yes)
                    {
                        lastWriteTime = fileLastWriteTime;
                        textBox1.Text = File.ReadAllText(currentFilePath);
                        this.Text = $"Éditeur de fichier texte [{Path.GetFileName(currentFilePath)}]";
                    }
                }
            }
        }

        private void OpenFile(string filePath)
        {
            try
            {
                string fileContent = File.ReadAllText(filePath);
                textBox1.Text = fileContent;

                currentFilePath = filePath;
                lastWriteTime = File.GetLastWriteTime(filePath);
                string filename = Path.GetFileName(filePath);
                this.Text = $"Éditeur de fichier texte [{filename}]";

                // Configurer le FileSystemWatcher pour le nouveau fichier
                fileWatcher.Path = Path.GetDirectoryName(filePath);
                fileWatcher.Filter = Path.GetFileName(filePath);
                fileWatcher.EnableRaisingEvents = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erreur lors de l'ouverture du fichier : " + ex.Message);
            }
        }

        private void personnaliserLaPoliceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (FontDialog fontDialog = new FontDialog())
            {
                fontDialog.Font = textBox1.Font;
                if (fontDialog.ShowDialog() == DialogResult.OK)
                {
                    textBox1.Font = fontDialog.Font;
                    Properties.Settings.Default.TextFont = textBox1.Font;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void personnaliserLaCouleurDuTexteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (ColorDialog colorDialog = new ColorDialog())
            {
                colorDialog.Color = textBox1.ForeColor;
                if (colorDialog.ShowDialog() == DialogResult.OK)
                {
                    textBox1.ForeColor = colorDialog.Color;
                    Properties.Settings.Default.TextColor = textBox1.ForeColor;
                    Properties.Settings.Default.Save();
                }
            }
        }
    }
}

